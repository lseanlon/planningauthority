var cheerio = require('cheerio');
var fs = require('fs');
var download = require('download');
var mkdirp = require('mkdirp');

require('shelljs/global');
var downloadPath = fs.readFileSync('./src/configuration/EntryPathInfo.txt', 'utf8');
var outputJson = {};
var getUrlEntry = function() {

    var paramValue = './src/parameter.xlsx';
    process.argv.forEach(function(val, index, array) {

        // node app  src/UrlList.txt 
        // eg. [0] node / [1] app/ [2]'src/UrlListInfo.txt'
        //console.log(index + ': ' + val);  

        //map file name
        if (index == 2 && val) {
            paramValue = val;
        }

    });

    return paramValue;
};

var excel2Json = require('node-excel-to-json');
var listResult = [];
node_xj = require("xls-to-json");
node_xj({
    input: getUrlEntry(), // input xls 
    output: "./src/parameter.json" // output json  
}, function(err, listResult) {
    if (err) {
        console.error(err);
        return;
    }
    populateAllResultList(listResult)


});

var finalResultExcelList = [],
    finalList = [];

function populateAllResultList(listResult) {

    console.log('populateAllResultList');

    listResult.forEach(function(_elem, index, collection) {
        // 'Local Planning Authority': '',
        //  'Starting URL': '',
        //  'Parameter 1': '',
        //  'Parameter 2': '',
        //  URL1: '-',
        //  URL2: '-' }

        if (!_elem || !_elem['Local Planning Authority']) {
            return;
        }
        console.log('_elem', _elem);


        //download part 
        var curLpa = _elem['Local Planning Authority'];
        var folderName = "./dist/" + curLpa;
        var fileName = folderName + "/file.html";




        if (!fs.existsSync(fileName)) {
            console.log("File missing. Skipped reading path ", fileName);

        } else {
            var urlPath = (_elem['Starting URL']);
            var param1 = (_elem['Parameter 1']);
            var param2 = (_elem['Parameter 2']);
            var curLpaUrl = (_elem['URL1']);



            try {


                //read file
                var htmlContent = fs.readFileSync(fileName, 'utf8');
                var $ = cheerio.load(htmlContent);

                //scrap file 
                var $resultList = $('.searchresult')
                $resultList.each(function(index, elem) {

                    var baseDomain = extractDomain(urlPath);
                    var link = "http://" + baseDomain + $(elem).find('a').attr('href');
                    var row1 = $(elem).find('a').text();
                    var row2 = $(elem).find('p.address').text();
                    var row3 = $(elem).find('p.metaInfo').text();
                    var curRow = { "curLpa": curLpa, "index": index, "link": link, "data": row1 + "\n" + row2 + "\n" + row3 }
                    curRow["data"] = curRow["data"].replace(/  /gi, ' ').replace(/\r/gi, '').replace(/\s+/g, " ");
                    curRow["row1"] = row1.replace(/  /gi, ' ').replace(/\r/gi, '').replace(/\s+/g, " ");
                    curRow["row2"] = row2.replace(/  /gi, ' ').replace(/\r/gi, '').replace(/\s+/g, " ");
                    curRow["row3"] = row3.replace(/  /gi, ' ').replace(/\r/gi, '').replace(/\s+/g, " ");
                    curRow["excelLpa"] = '=HYPERLINK("' + curLpaUrl + '","' + curLpa + '")';
                    curRow["excelResults"] = '=HYPERLINK("' + link + '",' + "C" + (index + 2) + ' )';
                    finalList.push(curRow);


                    var curRowExcel = {}
                    curRowExcel["LPA"] = curRow["excelLpa"]
                        // curRowExcel["Results"] = curRow["excelResults"]
                        // curRowExcel["excelResultData"] = curRow["data"];
                    curRowExcel["Results"] = curRow["data"] + "[linkseparator]" + curRow["link"]


                    var curRowExcel = {}
                    curRowExcel["LPA"] = curRow["excelLpa"]
                        // curRowExcel["Results"] = curRow["excelResults"]
                        // curRowExcel["excelResultData"] = curRow["data"];
                    curRowExcel["Results"] = curRow["data"] + "[linkseparator]" + curRow["link"]


                    var dataNonHtml = curRowExcel["Results"];
                    var foundStartIndex = dataNonHtml.indexOf('Received:');
                    var startReceivedIndex = dataNonHtml.indexOf('Received:') + 'Received'.length;
                    var endReceivedIndex = dataNonHtml.indexOf('|', startReceivedIndex);
                    //+ 1 +4 spaces for date exlude
                    if (foundStartIndex != -1 && endReceivedIndex != -1) {
                        curRowExcel["ReceivedDate"] = dataNonHtml.substring(startReceivedIndex + 5, endReceivedIndex);;
                    } else {
                        curRowExcel["ReceivedDate"] = '-';

                    }


                    var foundStartIndex = dataNonHtml.indexOf('Validated:');
                    var startValidatedIndex = dataNonHtml.indexOf('Validated:') + 'Validated'.length;
                    var endValidatedIndex = dataNonHtml.indexOf('|', startValidatedIndex);

                    if (foundStartIndex != -1 && endValidatedIndex != -1) {
                        curRowExcel["ValidatedDate"] = dataNonHtml.substring(startValidatedIndex + 5, endValidatedIndex);;
                    } else {
                        curRowExcel["ValidatedDate"] = '-';

                    }

                    console.log(curRowExcel);
                    finalResultExcelList.push(curRowExcel);
                });



            } catch (e) {
                console.log(e);

            }

        }

    });


    //write to txt as json
    //write as final excel

    writeIntoFiles();
}



function extractDomain(url) {
    var domain;
    //find & remove protocol (http, ftp, etc.) and get domain
    if (url.indexOf("://") > -1) {
        domain = url.split('/')[2];
    } else {
        domain = url.split('/')[0];
    }

    //find & remove port number
    domain = domain.split(':')[0];

    return domain;
}

//write into excel 
var json2xls = require('json2xls');

function writeIntoFiles() {

    console.log('finalList', finalList);
    var outtxtpath = './dist/finalResult.txt';
    fs.writeFileSync(outtxtpath, JSON.stringify(finalList));
    console.log("Write to txt ", outtxtpath);
    var outexcelpath = './dist/finalResult.xlsx';
    var xls = json2xls(finalResultExcelList);
    fs.writeFileSync(outexcelpath, xls, 'binary');
    console.log("Write to excel ", outexcelpath);
}
