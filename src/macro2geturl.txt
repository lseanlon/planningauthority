

Function GetFormula(rng As Range, Optional asR1C1 As Boolean = False) As String
    If asR1C1 Then
        GetFormula = rng.FormulaR1C1
    Else
        GetFormula = rng.Formula
    End If
End Function

 Function GetURL(cell As Range, Optional default_value As Variant)
 'Lists the Hyperlink Address for a Given Cell
 'If cell does not contain a hyperlink, return default_value
      If (cell.Range("A1").Hyperlinks.Count <> 1) Then
          GetURL = default_value
      Else
          GetURL = cell.Range("A1").Hyperlinks(1).Address
      End If
End Function



Sub ExtractLinks()
Dim i As Long
'skip header row
For i = 2 To 105
 
            
             Cells(i, 1).Select
             Cells(i, 10).Value = GetURL(Cells(i, 1), "-")
             Cells(i, 2).Select
             Cells(i, 11).Value = GetURL(Cells(i, 2), "-")
             
           
            

Next i

End Sub

