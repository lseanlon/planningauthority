

Function GetFormula(rng As Range, Optional asR1C1 As Boolean = False) As String
    If asR1C1 Then
        GetFormula = rng.FormulaR1C1
    Else
        GetFormula = rng.Formula
    End If
End Function


Sub Macro1()
Dim i As Long
For i = 1 To 166
            Columns(3).Font.Color = vbBlack
            Cells(i, 3).Font.Color = vbRed
            Cells(i, 3).Value = Cells(i, 3).Value
            
            
             Cells(i, 8).Value = GetFormula(Cells(i, 3)) 


             Cells(i, 3).Select
             SendKeys "{F2}", True
             SendKeys "{ENTER}", True
             
    ActiveWorkbook.Save
    ActiveWorkbook.Save

Next i

End Sub
