var page = require('webpage').create();
var loadInProgress = false;
var testindex = 0;
var system = require('system');
var param1, param2, param3, param4, param5;
var finalList = [],
    repeatStep = false;

//test
//phantomjsx PageDownloadMain.jsx "http://publicaccess.wycombe.gov.uk/idoxpa-web/search.do?action=advanced" "Application Type:" "Offices to dwelling houses"


if (system.args.length === 1) {
    //-console.log('Usage: google.js <some Query>');
    phantom.exit(1);
} else {
    q = system.args[1];
    param1 = system.args[2];
    param2 = system.args[3];
    param3 = system.args[4];
    param4 = system.args[5];
    param5 = system.args[6];
}


// Route "console.log()" calls from within the Page context to the main Phantom context (i.e. current "this")
page.onConsoleMessage = function(msg) {
    console.log(msg);
};

page.onAlert = function(msg) {
    console.log('alert!!> ' + msg);
};

page.onLoadStarted = function() {
    loadInProgress = true;
    console.log("load started");
};

page.onLoadFinished = function(status) {
    loadInProgress = false;
    if (status !== 'success') {
        console.log('Unable to access network');
        phantom.exit();
    } else {
        console.log("load finished");
    }
};

var steps = [
    function() {
        page.open(q);
    },

    function() {
        console.log('Answers:');
        page.injectJs("jquery1-11-1min.js");
        // page.render('example.png');
        setTimeout(function() {
            page.evaluate(function(param1, param2, param3, param4, param5) {


                var d = new Date();
                var month = d.getMonth() + 1;
                var day = d.getDate();

                var thisYearDate = (('' + day).length < 2 ? '0' : '') + day + '/' +
                    (('' + month).length < 2 ? '0' : '') + month + '/' +
                    (('' + day).length < 2 ? '0' : '') + d.getFullYear();
                var lastYearDate = (('' + day).length < 2 ? '0' : '') + day + '/' +
                    (('' + month).length < 2 ? '0' : '') + month + '/' +
                    (parseInt(d.getFullYear()) - 1);

                var fromDate = param3 ? param3 : lastYearDate;
                var toDate = param4 ? param4 : thisYearDate;

                var fromTime =new Date(fromDate).getTime()
                var toTime =new Date(toDate).getTime()
                if(fromTime > toTime){ 
                    console.log('Detected fromDate larger than toDate : Will SWAP values'  );
                }

                var dateLabel = param5 ? param5 : 'Date Received';



                var domSelstart = '#applicationReceivedStart'
                var domSelEnd = '#applicationReceivedEnd'

                var dateLabelSel = ' ';
                try {
                    dateLabelSel = 'label:contains(' + dateLabel + ':)'
                    domSelstart = '#' + $(dateLabelSel).eq(0).parent().find('input').attr('id')
                    domSelEnd = '#' + $(dateLabelSel).eq(1).parent().find('input').attr('id')

                } catch (e) {
                    console.log('custom label parameter not found for ', dateLabelSel);

                    domSelstart = '#applicationReceivedStart'
                    domSelEnd = '#applicationReceivedEnd'
                    console.log('Using default start date input', domSelstart);
                    console.log('Using default end date input', domSelEnd);
                }
                $(domSelstart).focus();
                $(domSelstart).val(fromDate);
                $(domSelstart).blur();
                $(domSelEnd).focus();
                $(domSelEnd).val(toDate);
                $(domSelstart).blur(); 


                var $caseType = $("label:contains('" + param1 + "')").next()
                var valOpt = $caseType.find('option:contains("' + param2 + '")').attr('value')
                $caseType.val(valOpt)



                $('input[value="Search"]')[0].click();
                $('input[value="Search"]').trigger('click');
            }, param1, param2, param3, param4, param5);
        }, 0);
    },
    function() {
        console.log('Answers2:');
        page.injectJs("jquery1-11-1min.js");
        // page.render('example.png');
        setTimeout(function() {
            page.evaluate(function() {

                // console.log('resultPage',$('body').html()); 
                //first page
                //chage to 100 results 

                var valOpt = $('#resultsPerPage option:contains("100")').attr('value')
                console.log('valOpt', valOpt);
                $('#resultsPerPage').val(valOpt)
                $('input[value="Go"]').trigger('click');

            });
        }, 0);
    },
    function() {
        console.log('Answers3:');
        page.injectJs("jquery1-11-1min.js");
        // page.render('example.png');
        setTimeout(function() {
            page.evaluate(function(finalList, repeatStep) {

                console.log($('body').html());
                //scrap data

                // var $resultList = $('.searchresult')
                // $resultList.each(function(index, elem) {

                //     var link = $(elem).find('a').attr('href');
                //     var row1 = $(elem).find('a').text();
                //     var row2 = $(elem).find('p.address').text();
                //     var row3 = $(elem).find('p.metaInfo').next().text();
                //     var current = { "index": index, "link": link, "data": row1 + "||" + row2 + "||" + row3 }

                //     finalList.push(current);
                // });  

                if ($('a[class="next"]').length > 0) {
                    repeatStep = true;
                    $('a[class="next"]').eq(0)[0].click();
                } else {
                    repeatStep = false;
                }


            }, finalList, repeatStep);
        }, 0);
    },
    function() {

        console.log('finalList=' + JSON.stringify(finalList));
        console.log('Exiting');
    }
];

interval = setInterval(function() {
    if (!loadInProgress && typeof steps[testindex] == "function") {
        console.log("step " + (testindex + 1));
        steps[testindex]();

        if (repeatStep) {
            testindex;
        } else {
            testindex++;
        }

    }
    if (typeof steps[testindex] != "function") {
        console.log("test complete!");
        phantom.exit();
    }
}, 500);
