var cheerio = require('cheerio');
var fs = require('fs');
var download = require('download');
var mkdirp = require('mkdirp');

require('shelljs/global');
var downloadPath = fs.readFileSync('./src/configuration/EntryPathInfo.txt', 'utf8');
var outputJson = {};
var getUrlEntry = function() {

    var paramValue = './src/parameter.xlsx';
    process.argv.forEach(function(val, index, array) {

        // node app  src/UrlList.txt 
        // eg. [0] node / [1] app/ [2]'src/UrlListInfo.txt'
        //console.log(index + ': ' + val);  

        //map file name
        if (index == 2 && val) {
            paramValue = val;
        }

    });

    return paramValue;
};

var excel2Json = require('node-excel-to-json');
var listResult = [];
node_xj = require("xls-to-json");
node_xj({
    input: getUrlEntry(), // input xls 
    output: "./src/parameter.json" // output json  
}, function(err, listResult) {
    if (err) {
        console.error(err);
        return;
    }
    downloadAllResultList(listResult)


});


function downloadAllResultList(listResult) {

    console.log('downloadAllResultList');

    listResult.forEach(function(_elem, index, collection) {
        // 'Local Planning Authority': '',
        //  'Starting URL': '',
        //  'Parameter 1': '',
        //  'Parameter 2': '',
        //  URL1: '-',
        //  URL2: '-' }

        if (!_elem || !_elem['Local Planning Authority']) {
            return;
        }
        console.log('_elem', _elem);


        //download part 
        var folderName = "./dist/" + _elem['Local Planning Authority'];
        var fileName = folderName + "/file.html";

        mkdirp(folderName, function(err) {
            if (err) return cb(err);
            console.log("/created folder path", folderName);
        });


            var urlPath = (_elem['Starting URL']);
            var param1 = (_elem['Parameter 1']);
            var param2 = (_elem['Parameter 2']);
            var param3 = (_elem['Received From Date']);
            var param4 = (_elem['Received To Date']); 
 
  
 

        if (fs.existsSync(fileName)) {
            console.log("File downloaded. Skipped downloading path ", fileName);

        } else {
            var urlPath = (_elem['Starting URL']);
            var param1 = (_elem['Parameter 1']);
            var param2 = (_elem['Parameter 2']);
            var param3 = (_elem['Received From Date']);
            var param4 = (_elem['Received To Date']); 
            var param5 = (_elem['Date Label']); 
 
  

            var commandRun = 'phantomjs PageDownloadMain.js "' + urlPath + '" "' + param1 + '" "' + param2 + '" "'+param3 + '" "'+param4+ '" '+ ' "'+param5+ '" ';
 
            try {

                console.log("commandRun", commandRun);
                exec(commandRun, function(status, output) {
                    console.log('Command  status:', status);
                    console.log('Command output:');

                    console.log('Done download urlPath', urlPath);

                    if (!urlPath || !output) {
                        return;
                    }

                    fs.writeFileSync(fileName, output);
                    console.log('Write to fileName ', fileName);



                });

            } catch (e) {
                console.log(e);

            }

        }

    });

}
